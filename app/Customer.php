<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Customer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer';



    public static function customerLogin($email,$password){
       // echo md5($password);
       // select `email`, `password` from `customer` where `email` = 'abc@test.lk' and `password` = '$2y$10$g1eKeWrA/M6EbvTPpfBvH.dqvD41UYShXCjw62YDVg8foabAsVOwK'
        return  DB::table('customer')
            ->select('id','first_name','last_name','email','password')
            ->where('email', '=' ,$email)
            ->where('password', '=' ,$password)
            ->get();

    }


}
