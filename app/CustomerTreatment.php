<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class CustomerTreatment extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'customer_treatment';

    public static function getCustomerTreatment(){


        return  DB::table('customer_treatment')
            ->join('customer', 'customer.id', '=', 'customer_treatment.customer_id')
            ->join('treatment','treatment.id', '=', 'customer_treatment.treatment_id')
            ->select(
                'customer.id AS customer_id','customer.first_name',
                'customer.last_name','customer.title',
                'treatment.id AS treatment_id', 'treatment.treatment_type',
                'customer_treatment.id','customer_treatment.treatment_date','customer_treatment.treatment_for',
                'customer_treatment.treatment_description','customer_treatment.before_treatment_image'
                )->paginate(20);


    }


    public static function getCustomerTreatmentById($id){


        return  DB::table('customer_treatment')
            ->join('customer', 'customer.id', '=', 'customer_treatment.customer_id')
            ->join('treatment','treatment.id', '=', 'customer_treatment.treatment_id')
            ->select(
                'customer.id AS customer_id','customer.first_name',
                'customer.last_name','customer.title',
                'treatment.id AS treatment_id', 'treatment.treatment_type',
                'customer_treatment.id','customer_treatment.treatment_date','customer_treatment.treatment_for',
                'customer_treatment.treatment_description','customer_treatment.before_treatment_image'
            )
            ->where('customer_treatment.id', '=', $id)
            ->get();


    }



    public static function getAllCustomerTreatmentByCustomerId($id){


        return  DB::table('customer_treatment')
            ->join('customer', 'customer.id', '=', 'customer_treatment.customer_id')
            ->join('treatment','treatment.id', '=', 'customer_treatment.treatment_id')
            ->select(
                'customer.id AS customer_id','customer.first_name',
                'customer.last_name','customer.title',
                'treatment.id AS treatment_id', 'treatment.treatment_type',
                'customer_treatment.id','customer_treatment.treatment_date','customer_treatment.treatment_for',
                'customer_treatment.treatment_description','customer_treatment.before_treatment_image'
            )
            ->where('customer.id', '=', $id)
            ->get();


    }


    public static function getCustomerTreatmentByName($url_param){


      return   DB::table('customer_treatment')
            ->join('customer', 'customer.id', '=', 'customer_treatment.customer_id')
            ->join('treatment','treatment.id', '=', 'customer_treatment.treatment_id')
            ->select(
                'customer.id AS customer_id','customer.first_name',
                'customer.last_name','customer.title',
                'treatment.id AS treatment_id', 'treatment.treatment_type',
                'customer_treatment.id','customer_treatment.treatment_date','customer_treatment.treatment_for',
                'customer_treatment.treatment_description','customer_treatment.before_treatment_image'
            )

            ->where('treatment.treatment_type', 'like', '%' . $url_param . '%')

            ->get();


    }


}
