<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});



// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


/*
Home page
 */

Route::get('/', 'DashboardController@index');
Route::get('/home', 'DashboardController@index');


//Customer

get('customer', 'CustomerController@index');
get('customer/create', 'CustomerController@create');
post('customer/create', 'CustomerController@create');
get('customer/view/{id}', 'CustomerController@viewCustomerById');
get('customer/edit/{id}', 'CustomerController@edit');
post('customer/edit', 'CustomerController@edit');
post('customer/delete', 'CustomerController@deleteCustomerById');

get('customer/create_customer_treatment/{id}', 'CustomerController@createCustomerTreatment');
post('customer/create_customer_treatment', 'CustomerController@createCustomerTreatment');

get('customer/customer_treatments', 'CustomerController@listCustomerTreatments');
get('customer/customer_treatments_details/{id}', 'CustomerController@showCustomerTreatmentsById');
post('customer/delete_customer_treatment', 'CustomerController@deleteCustomerTreatmentById');
//search customer
post('customer', 'CustomerController@findCustomerByName');
//search customer treatments
post('customer/customer_treatments', 'CustomerController@findCustomerTreatmentByTreatmentName');

//Treatments

get('treatment', 'TreatmentController@index');
get('treatment/create', 'TreatmentController@create');
post('treatment/create', 'TreatmentController@create');
post('treatment/details', 'TreatmentController@viewTreatmentById');
post('treatment/update', 'TreatmentController@update');
post('treatment/delete', 'TreatmentController@deleteTreatmentById');

//search treatment
post('treatment', 'TreatmentController@findTreatmentByName');

//Customer Logout
get('customer/logout', 'CustomerController@customerLogout');



