<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
class DashboardController extends Controller
{

    public  function __construct(){

         if(!session()->get('role')){
             $this->middleware('auth');
         }

    }


    public function index(){
       // return "home";
      //  return Auth::user()->name;
        return  view('dash.dashboard');
    }
}


