<?php

namespace App\Http\Controllers;

use App\Treatment;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Mockery\CountValidator\Exception;

class TreatmentController extends Controller
{


    public  function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $treatment = Treatment::all();
        return view('treatment.listTreatment',['treatment'=>$treatment]);

    }



    /**
     * create a new Treatment.
     *
     * @return Response
     */

    public function create(Request $request){

//        $this->validate($request, [
//            'title' => 'required|unique:posts|max:255',
//            'body' => 'required',
//        ]);

        $treatment_create =   $request->input('treatment_create');

        if($treatment_create == 1) {

            $treatment_data = array();
            $treatment_data['treatment_type'] = $request->input('treatment_name');

            try{

                if(DB::table('treatment')->insert($treatment_data)){

                    $request->session()->flash('alert-success', 'Treatment added successfully');
                    return redirect()->action('TreatmentController@index');

                }else{
                    return view('treatment.addTreatment');
                }

            }catch (\Exception $e){

                return view('treatment.addTreatment',['errors'=>$e->getMessage()]);
            }




        }else{
            return view('treatment.addTreatment');
        }


    }

    public function viewTreatmentById(Request $request){

        $treatment_id = $request->input('treat_id');

        return  $treatment_id_result = Treatment::find($treatment_id);

    }


    public function update(Request $request){

        $treatment_edit    =   $request->input('treatment_edit');

        if($treatment_edit == 1){

            $treatment_id    =   $request->input('treat_id');
            $treatment_data = array();
            $treatment_data['treatment_type'] = $request->input('treatment_name');

            if(DB::table('treatment')->where('id','=', $treatment_id)->update($treatment_data)){
                return "1";
            }else{
               return "0";
            }

        }else{
            return "0";
        }

    }



    public function deleteTreatmentById(Request $request){

        $treatment_id = $request->input('treat_id');

        if(DB::table('treatment')->where('id', '=',$treatment_id )->delete()){

            return $treatment_id;

        }else{
            return "0";

        }

    }



    public  function findTreatmentByName(Request $request){


        $url_param = $_POST;
        $treatment = DB::table('treatment');
        if(!empty($url_param)) {
            if (count($url_param) > 1) {
                if ($url_param['treatment_name'] != '') {
                    $treatment->where('treatment_type', "like", '%' . $url_param['treatment_name'] . '%');
                }
            }
        }

        $treatment->select('treatment_type','id');
        $treatment = $treatment->get();


        return view('treatment.listTreatment',['treatment'=>$treatment]);



    }

}
