<?php

namespace App\Http\Controllers;
use Mail;
use App\Customer;
use App\Treatment;
use App\CustomerTreatment;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class CustomerController extends Controller
{

    public  function __construct(){

        if(!session()->get('role')){
            $this->middleware('auth');
        }


    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        //$customers = Customer::all();
        $customers = DB::table('customer')->paginate(20);
        return view('customer.listCustomer',['customers'=>$customers]);

    }

    /**
     * create a new customer.
     *
     * @return Response
     */

    public function create(Request $request){

        $customer_create    =   $request->input('customer_create');

        if($customer_create == 1){

//
//            $data = array('first_name'=>"sdfsdf",'last_name'=>"fffff");
//
//            Mail::send('emails.customer_details',$data, function ($message)  {
//                $message->from('info@tutorsync.com', 'Tutor Sync');
//                $message->subject('In-Home Tutor Application Approved');
//                $message->to('gayanramyakumara@gmail.com');
//            });
//           exit;


            $customer_data = array();

            $customer_data['title'] = $request->input('customer_title');
            $customer_data['first_name'] = $request->input('customer_first_name');
            $customer_data['last_name'] = $request->input('customer_last_name');
            $customer_data['address'] = $request->input('customer_address');
            $customer_data['country'] = $request->input('customer_country');
            $customer_data['email'] = $request->input('customer_email');
            $customer_data['date_of_birth'] = $request->input('customer_date_of_birth');
            $customer_data['password'] = md5($request->input('customer_password'));
            $customer_data['admin_id'] = $request->input('customer_created_admin_id');
            $customer_data['phone_mobile'] =  preg_replace('/[^0-9]+/', '', $request->input('customer_mobile'));
            $customer_data['phone_home'] =  preg_replace('/[^0-9]+/', '', $request->input('customer_home'));
            $customer_data['is_active'] = 1;

            try{


                if(DB::table('customer')->insert($customer_data)){
                    $request->session()->flash('alert-success', 'Customer added successfully');
                    return redirect()->action('CustomerController@index');
                }else{
                    return view('customer.addCustomer');
                }

            }catch (\Exception $e){

            return view('customer.addCustomer',['errors'=>$e->getMessage()]);

            }


        }else{
            return view('customer.addCustomer');
        }

    }


    public function findCustomerByName(Request $request){
        $url_param = $_POST;
        $customers = DB::table('customer');
        if(!empty($url_param)) {
            if (count($url_param) > 1) {
                if ($url_param['customer_name'] != '') {
                    $customers->where(DB::raw("CONCAT(first_name, ' ', last_name)"), "like", '%' . $url_param['customer_name'] . '%');
                }
            }
        }

        $customers->select('first_name','id','last_name','email','title','country','phone_home','phone_mobile','address');
        $customers = $customers->paginate(20);

        return view('customer.listCustomer',['customers'=>$customers]);
    }


    public function viewCustomerById($id){

        $customer_result = Customer::find($id);

        $customer_treatments_details =  CustomerTreatment::getAllCustomerTreatmentByCustomerId($id);

        return view('customer.viewCustomerById',['customer_details'=>$customer_result,'customer_treatments_details'=>$customer_treatments_details]);

    }

    public function deleteCustomerById(Request $request){
        $customer_id = $request->input('customer_id');

        if(DB::table('customer')->where('id', '=',$customer_id )->delete()){

          //  $request->session()->flash('alert-success', 'Customer deleted successfully');
            //return redirect()->action('CustomerController@index');

            return $customer_id;

        }else{
            return "0";

            //return redirect()->action('CustomerController@index');

        }

    }

     public function edit(Request $request,$id=false){


         $customer_edit    =   $request->input('customer_edit');
         if($customer_edit == 1){


             $customer_id    =   $request->input('customer_id');

             $customer_data = array();

             $customer_data['title'] = $request->input('customer_title');
             $customer_data['first_name'] = $request->input('customer_first_name');
             $customer_data['last_name'] = $request->input('customer_last_name');
             $customer_data['address'] = $request->input('customer_address');
             $customer_data['country'] = $request->input('customer_country');
             $customer_data['email'] = $request->input('customer_email');
             $customer_data['date_of_birth'] =  $request->input('customer_date_of_birth');
             $customer_data['admin_id'] = $request->input('customer_created_admin_id');
             $customer_data['phone_mobile'] =  preg_replace('/[^0-9]+/', '', $request->input('customer_mobile'));
             $customer_data['phone_home'] =  preg_replace('/[^0-9]+/', '', $request->input('customer_home'));
             $customer_data['is_active'] = 1;

             if(DB::table('customer')->where('id','=', $customer_id)->update($customer_data)){

                 $request->session()->flash('alert-success', 'Customer update successfully');
                 return redirect()->action('CustomerController@index');

             }else{

                 $customer_result = Customer::find($customer_id);
                 return view('customer.editCustomer',['customer_result'=>$customer_result]);
             }

         }else{
             $customer_result = Customer::find($id);
             return view('customer.editCustomer',['customer_result'=>$customer_result]);
         }

     }


    public function createCustomerTreatment(Request $request,$id=false){

        $treatment =  Treatment::all();

        $customer_treatment_create   =   $request->input('customer_treatment_create');

        if($customer_treatment_create == 1){

            $customer_treatment_data = array();

            $customer_treatment_data['treatment_date'] = date("Y-m-d", strtotime($request->input('customer_treatment_date')));
            $customer_treatment_data['treatment_for'] = $request->input('customer_treatment_for');
            $customer_treatment_data['treatment_description'] = $request->input('customer_treatment_description');
            $customer_treatment_data['customer_id'] = $request->input('customer_id');
            $customer_treatment_data['treatment_id'] = $request->input('treatment_id');

            if ($request->hasFile('customer_treatment_before_image')) {

                $destinationPath = 'uploads/before_treatment';
                $extension = $request->file('customer_treatment_before_image')->getClientOriginalExtension();
                $fileName = "img_before_date".rand(11111,99999).'.'.$extension; // renameing image
                //$request->file('customer_treatment_before_image')->move($destinationPath, $fileName); // uploading file to given path
                $request->file('customer_treatment_before_image')->move(storage_path('app/uploads/before_treatment'), $fileName); // uploading file to given path

                $customer_treatment_data['before_treatment_image'] = $fileName;

            }

            if(DB::table('customer_treatment')->insert($customer_treatment_data)){

                if( $request->session()->get('role')==1){

                    $request->session()->flash('alert-success', 'Customer Treatment added successfully');
                    return redirect()->action('CustomerController@index');

                }else{

                    $request->session()->flash('alert-success', 'Customer Treatment added successfully');
                   //return redirect()->route('profile', [$user]);
                     $customer_id = session()->get('customer_id');
                  //return redirect()->route('customer/view',[$customer_id]);
                    return redirect()->action('CustomerController@viewCustomerById', $customer_id);

                }



            }else{

                return view('customer.addCustomerTreatment',['customer_id'=>$id,'treatment_list'=>$treatment]);
            }

        }else{

            return view('customer.addCustomerTreatment',['customer_id'=>$id,'treatment_list'=>$treatment]);

        }

    }


    public function listCustomerTreatments(){

        $customer_treatments =  CustomerTreatment::getCustomerTreatment();
        return view('customer.customerTreatmentList',['customer_treatments'=>$customer_treatments]);

    }


    public function showCustomerTreatmentsById($id){

        $customer_treatments_details =  CustomerTreatment::getCustomerTreatmentById($id);
        return view('customer.customerTreatmentDetails',['customer_treatments_details'=>$customer_treatments_details]);

    }





    public function deleteCustomerTreatmentById(Request $request){

        $treatment_id = $request->input('treatment_id');
        $set_before_treatment_image = $request->input('set_before_treatment_image');

        $exists = Storage::disk('local')->has('uploads/before_treatment/'.$set_before_treatment_image);

        if(DB::table('customer_treatment')->where('id', '=',$treatment_id )->delete()){

            if($exists){
                Storage::delete('uploads/before_treatment/'.$set_before_treatment_image);
            }

            return $treatment_id;

        }else{
            return "0";

        }

    }




    public function findCustomerTreatmentByTreatmentName(Request $request){
        $url_param = $_POST;

        $url_param =  $url_param['treatment_name'];

        $customer_treatments =  CustomerTreatment::getCustomerTreatmentByName($url_param);

        return view('customer.customerTreatmentList',['customer_treatments'=>$customer_treatments]);

    }


    public function customerLogout(Request $request){

      $request->session()->flush();
      return redirect()->action('Auth\AuthController@getLogin');


    }


}
