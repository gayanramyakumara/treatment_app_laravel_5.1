


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!--[if gt IE 8]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    <title>Form Elements - Leo - Premium Admin Template</title>
    <link rel="icon" type="image/ico" href="{{ URL::asset('assets/favicon.ico') }}"/>

    <link href="{{ URL::asset('assets/css/stylesheets.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/mystyles.css') }}" rel="stylesheet" type="text/css" />

    <!--[if lte IE 7]>
    <script type='text/javascript' src="{{ URL::asset('assets/js/other/lte-ie7.js') }}"></script>
    <![endif]-->

    <script type='text/javascript' src="{{ URL::asset('assets/js/jquery/jquery.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/jquery/jquery-ui-1.10.3.custom.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/jquery/jquery-migrate.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/jquery/globalize.js') }}"></script>

    <script type='text/javascript' src="{{ URL::asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/cookies/jquery.cookies.2.2.0.min.js') }}"></script>

    <script type='text/javascript' src="{{ URL::asset('assets/js/validationengine/languages/jquery.validationEngine-en.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/validationengine/jquery.validationEngine.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/maskedinput/jquery.maskedinput.min.js') }}"></script>


    <script type='text/javascript' src="{{ URL::asset('assets/js/select2/select2.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/uniform/jquery.uniform.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/tagsinput/jquery.tagsinput.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/multiselect/jquery.multi-select.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/ibutton/jquery.ibutton.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/colorpicker/colorpicker.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/timepicker/jquery-ui-timepicker-addon.js') }}"></script>


    <script type='text/javascript' src="{{ URL::asset('assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/scrollup/jquery.scrollUp.min.js') }}"></script>

    <script type='text/javascript' src="{{ URL::asset('assets/js/plugins.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/actions.js') }}"></script>
</head>
<body>
<div id="wrapper" class="screen_wide sidebar_off">
    <div id="layout">
        <div id="content">
            <div class="wrap nm">

                <div class="signin_block">
                    <div class="row">


                        @foreach($errors->all() as $error)

                            <div class="alert alert-danger">
                                <strong>Oh snap!</strong> {{ $error }}
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                        @endforeach

                        <div class="block">
                            <div class="head">
                                <h2>Sign in</h2>
                                <ul class="buttons">
                                    <li><a href="{{ url('auth/register') }}" class="tip" title="Register"><i class="i-warning"></i></a></li>
                                    <li><a href="{{ url('password/email') }}" class="tip" title="Forget your password?"><i class="i-locked"></i></a></li>
                                </ul>
                            </div>

                            {!!Form::open(array('action' => 'Auth\PasswordController@postReset','method'=>'POST'))!!}

                            {!! csrf_field() !!}
                            <input type="hidden" name="token" value="{{ $token }}">



                            <div class="content np">

                                <div class="controls-row">

                                    <div class="col-md-3">
                                    {!! Form::label('email', 'Email', array('for' => 'email','class'=>'control-label'))!!}
                                    </div>

                                    <div class="col-md-9">
                                        {!! Form::text('email', old('email') , array('class' => 'form-control', 'id'=>'email','placeholder'=>'Email','autofocus'=>''))!!}
                                    </div>

                                </div>


                                <div class="controls-row">

                                    <div class="col-md-3">
                                        {!! Form::label('password', 'Password', array('for' => 'password','class'=>'control-label'))!!}
                                    </div>

                                    <div class="col-md-9">
                                        {!! Form::password('password',   array('class' => 'form-control', 'id'=>'password','placeholder'=>'*********'))!!}

                                    </div>

                                </div>

                                    <div class="controls-row">

                                        <div class="col-md-3">
                                {!! Form::label('password_confirmation', 'Confirm password', array('for' => 'password_confirmation','class'=>'control-label'))!!}
                                        </div>

                                        <div class="col-md-9">
                                            {!! Form::password('password_confirmation',   array('class' => 'form-control', 'id'=>'password_confirmation','placeholder'=>'*********'))!!}

                                        </div>

                                    </div>




                            </div>
                            <div class="footer">

                                <div class="side fr">
                                    <button class="btn btn-primary" type="submit">Reset Password</button>
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

</body>
</html>




{{--@foreach($errors->all() as $error)--}}

                                {{--<div class="form-group has-feedback">--}}
                                    {{--<div class="alert alert-danger alert-white rounded no-margin">--}}
                                        {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>--}}
                                        {{--<div class="icon"><i class="fa fa-fire"></i></div>--}}
                                        {{--<strong>Oh snap!</strong> {{ $error }}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}

                        {{--</div>--}}



                        {{--{!!Form::open(array('action' => 'Auth\PasswordController@postReset','method'=>'POST'))!!}--}}

                            {{--{!! csrf_field() !!}--}}
                            {{--<input type="hidden" name="token" value="{{ $token }}">--}}


                        {{--<div class="form-group has-feedback">--}}

                            {{--{!! Form::label('email', 'Email', array('for' => 'email','class'=>'control-label'))!!}--}}
                            {{--{!! Form::text('email', old('email') , array('class' => 'form-control', 'id'=>'email','placeholder'=>'Email','autofocus'=>''))!!}--}}
                            {{--<i class="fa fa-user text-info form-control-feedback"></i>--}}
                        {{--</div>--}}


                        {{--<div class="form-group has-feedback">--}}

                            {{--{!! Form::label('password', 'Password', array('for' => 'password','class'=>'control-label'))!!}--}}
                            {{--{!! Form::password('password',   array('class' => 'form-control', 'id'=>'password','placeholder'=>'*********'))!!}--}}

                            {{--<i class="fa fa-key form-control-feedback"></i>--}}
                        {{--</div>--}}

                        {{--<div class="form-group has-feedback">--}}
                            {{--{!! Form::label('password_confirmation', 'Confirm password', array('for' => 'password_confirmation','class'=>'control-label'))!!}--}}
                            {{--{!! Form::password('password_confirmation',   array('class' => 'form-control', 'id'=>'password_confirmation','placeholder'=>'*********'))!!}--}}

                            {{--<i class="fa fa-key form-control-feedback"></i>--}}
                        {{--</div>--}}

                            {{--<div>--}}
                                {{--<button type="submit"  class="btn btn-danger btn-lg btn-block">--}}
                                    {{--Reset Password--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--{!! Form::close() !!}--}}


