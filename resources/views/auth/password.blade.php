

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!--[if gt IE 8]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    <title>Form Elements - Leo - Premium Admin Template</title>
    <link rel="icon" type="image/ico" href="{{ URL::asset('assets/favicon.ico') }}"/>

    <link href="{{ URL::asset('assets/css/stylesheets.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/mystyles.css') }}" rel="stylesheet" type="text/css" />

    <!--[if lte IE 7]>
    <script type='text/javascript' src="{{ URL::asset('assets/js/other/lte-ie7.js') }}"></script>
    <![endif]-->

    <script type='text/javascript' src="{{ URL::asset('assets/js/jquery/jquery.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/jquery/jquery-ui-1.10.3.custom.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/jquery/jquery-migrate.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/jquery/globalize.js') }}"></script>

    <script type='text/javascript' src="{{ URL::asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/cookies/jquery.cookies.2.2.0.min.js') }}"></script>

    <script type='text/javascript' src="{{ URL::asset('assets/js/validationengine/languages/jquery.validationEngine-en.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/validationengine/jquery.validationEngine.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/maskedinput/jquery.maskedinput.min.js') }}"></script>


    <script type='text/javascript' src="{{ URL::asset('assets/js/select2/select2.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/uniform/jquery.uniform.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/tagsinput/jquery.tagsinput.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/multiselect/jquery.multi-select.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/ibutton/jquery.ibutton.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/colorpicker/colorpicker.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/timepicker/jquery-ui-timepicker-addon.js') }}"></script>


    <script type='text/javascript' src="{{ URL::asset('assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/scrollup/jquery.scrollUp.min.js') }}"></script>

    <script type='text/javascript' src="{{ URL::asset('assets/js/plugins.js') }}"></script>
    <script type='text/javascript' src="{{ URL::asset('assets/js/actions.js') }}"></script>
</head>
<body>
<div id="wrapper" class="screen_wide sidebar_off">
    <div id="layout">
        <div id="content">
            <div class="wrap nm">

                <div class="signin_block">
                    <div class="row">


                        @foreach($errors->all() as $error)

                            <div class="alert alert-danger">
                                <strong>Oh snap!</strong> {{ $error }}
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                        @endforeach

                        <div class="block">
                            <div class="head">
                                <h2>Reset Your Password</h2>
                                <ul class="buttons">
                                    <li><a href="{{ url('auth/register') }}" class="tip" title="Register"><i class="i-warning"></i></a></li>
                                    <li><a href="{{ url('auth/login') }}" class="tip" title="Sign In?"><i class="i-locked"></i></a></li>
                                </ul>
                            </div>

                            {!!Form::open(array('action' => 'Auth\PasswordController@postEmail','method'=>'POST'))!!}

                            {!! csrf_field() !!}
                            <div class="content np">

                                <div class="controls-row">
                                    <div class="col-md-3">
                                {!! Form::label('email', 'Email', array('for' => 'email','class'=>'control-label'))!!}
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::text('email', old('email') , array('class' => 'form-control', 'id'=>'email','placeholder'=>'Email','autofocus'=>''))!!}
                                    </div>
                                </div>



                            </div>
                            <div class="footer">

                                <div class="side fr">
                                    <button class="btn btn-primary" type="submit">Sign in</button>
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

</body>
</html>





