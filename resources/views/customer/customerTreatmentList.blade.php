@extends('layouts.master')

@section('title', 'Customer Treatments')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Customers  </h1>
                <ul class="breadcrumb">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>

                        <li><a href="{{ url('customer') }}">Customer Treatments</a></li>


                    <li class="active">Show Customer Treatments</li>
                </ul>
            </div>

            <div class="search">
                {!!Form::open(array('action' => 'CustomerController@findCustomerTreatmentByTreatmentName','id' => 'find_customer_treatment_by_name', 'class' => 'form','role'=>'form'))!!}

                {!! Form::text('treatment_name',Input::get("treatment_name"), array('class' => 'form-control', 'placeholder'=>'Treatment Name...','style'=>'color: #67667B;font-size: 14px;'))!!}

                 <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong class="success_msg">Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif

                    <div class="success_msg_append" style="display: none"> </div>

                    <div class="block">



                        <div class="head">
                            <h2>Show All Customer Treatments  </h2>
                            <div class="side fl">

                            </div>
                        </div>

                        <div class="content np">

                            {{--<div class="content np table-sorting">--}}
                            <div class="content ">

                                <table cellpadding="0" cellspacing="0" width="100%">
                                {{--<table cellpadding="0" cellspacing="0" width="100%" class="simple_sort">--}}
                                    <thead>
                                    <tr>

                                        <th width="5%">ID</th>
                                        <th width="15%">Customer Name</th>
                                        <th width="25%">Treatment</th>
                                        <th width="25%">Treatment For</th>
                                        <th width="15%">Treatment Date</th>
                                        <th width="25%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($customer_treatments as $customer_treatments_value)

                                        <tr id="row_subtuid_{{$customer_treatments_value->id}}">

                                            <td><input type="checkbox" name="checkbox"/></td>

                                            <td>
                                                <a href="{{url('customer/customer_treatments_details/'.$customer_treatments_value->id)}}" style="text-transform: capitalize;">
                                                    {{$customer_treatments_value->first_name." ".$customer_treatments_value->last_name}}
                                                </a>
                                            </td>
                                            <td>  {{$customer_treatments_value->treatment_type }}</td>
                                            <td>  {{$customer_treatments_value->treatment_for }}</td>
                                            <td>
                                                <?php
                                                $treatment_date = explode(" ",$customer_treatments_value->treatment_date);
                                                ?>
                                                {{ $treatment_date[0]  }}
                                            </td>

                                            <td>

                                                <a href="{{url('customer/customer_treatments_details/'.$customer_treatments_value->id)}}" style="text-transform: capitalize;">
                                                    <span class=" i-file text-success"></span>
                                                </a>

                                                <a href="#bModal" data-toggle="modal" id="{{$customer_treatments_value->id}}" onclick="return  setTreatmentById('{{csrf_token()}}','{{$customer_treatments_value->id}}','{{$customer_treatments_value->before_treatment_image}}');" class="delete_customer_model">
                                                    <span class="i-trashcan text-danger"></span>
                                                </a>

                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>
                                </table>

                            </div>

                        </div>

                    </div>

                </div>



                <div class="col-md-4"></div>

                <div class="col-md-4">
                    {!! $customer_treatments->render() !!}
                </div>


                <div class="col-md-4"></div>



            </div>

        </div>



        <!-- EOF Bootrstrap default dialog -->
        <!-- Bootrstrap modal -->
        <div class="modal fade" id="bModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel2">Delete Customer Treatment</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure want to Delete this Customer Treatment ?</p>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" class="treatment_id" id="set_treatment_id"/>
                        <input type="hidden" class="set_before_treatment_image" id="set_before_treatment_image"/>
                        <a href="#" class="btn btn-danger" id="delete_cus" data-dismiss="modal" aria-hidden="true" onclick="return  deleteCustomerTreatmentById('{{csrf_token()}}');" >Delete</a>
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">

            function setTreatmentById(token,treatment_id,before_image) {
                    if(treatment_id){
                        $('#set_treatment_id').val(treatment_id);
                        $('#set_before_treatment_image').val(before_image);
                    }else{
                        return false;
                    }
            }

            function deleteCustomerTreatmentById(token) {

                  var treatment_id = $('#set_treatment_id').attr('value');
                  var set_before_treatment_image = $('#set_before_treatment_image').attr('value');

                    $.post('{{url('customer/delete_customer_treatment')}}', {treatment_id: treatment_id, 'set_before_treatment_image':set_before_treatment_image,'_token': token}, function (data) {

                        if(data){

                            $('#row_subtuid_'+data).fadeOut(1000);

                            $('.success_msg_append').html(" <div class='alert alert-success'> <strong class='success_msg'> Delete has been success </strong> <button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
                            $('.success_msg_append').css('display','block');
                            $('.success_msg_append').fadeOut(6000);

                            location.reload();

                        }else{
                            return false;
                        }

                });
            }

        </script>

@stop