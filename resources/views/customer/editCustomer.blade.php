@extends('layouts.master')

@section('title', 'Update Customer')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Customer  </h1>
                <ul class="breadcrumb">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    <li><a href="{{ url('customer') }}">Customer</a></li>
                    <li class="active">Update Customer</li>
                </ul>
            </div>


        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="block">
                        <div class="head">
                            <h2>Update customer  </h2>
                            <div class="side fr">

                            </div>
                        </div>
                        <div class="content np">


                            {!!Form::open(array('action' => 'CustomerController@edit','id' => 'validate', 'class' => 'update_new_patient','role'=>'form','onclick'=>'javascript'))!!}

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_title', ' Title:', array('for' => 'customer_title'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::select('customer_title',array('' => 'All','mr' => 'Mr', 'mrs' => 'Mrs', 'miss' => 'Miss', 'rev' => 'Rev',  'dr' => 'Dr','mest' => 'Mest', 'baby' => 'Baby'), Input::get('customer_title'), ['class' => 'validate[required] form-control','id'=>'customer_title']) !!}
                                </div>
                            </div>
                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_first_name', ' First Name:', array('for' => 'customer_first_name'))!!}
                                </div>
                                <div class="col-md-9">
                                    <?php
                                        if($customer_result->first_name){
                                            $cus_first_name = $customer_result->first_name;
                                        }else{
                                            $cus_first_name = Input::get("customer_first_name");
                                        }
                                    ?>
                                    {!! Form::text('customer_first_name',$cus_first_name, array('class' => 'validate[required] form-control', 'id'=>'customer_first_name','placeholder'=>''))!!}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_last_name', ' Last Name:', array('for' => 'customer_last_name'))!!}
                                </div>
                                <div class="col-md-9">
                                    <?php
                                    if($customer_result->last_name){
                                        $cus_last_name = $customer_result->last_name;
                                    }else{
                                        $cus_last_name = Input::get("customer_last_name");
                                    }
                                    ?>
                                    {!! Form::text('customer_last_name',$cus_last_name, array('class' => 'validate[required] form-control', 'id'=>'customer_last_name','placeholder'=>''))!!}
                                </div>
                            </div>




                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_address', ' Address:', array('for' => 'customer_address'))!!}
                                </div>
                                <div class="col-md-9">
                                    <?php
                                    if($customer_result->address){
                                        $cus_address = $customer_result->address;
                                    }else{
                                        $cus_address = Input::get("customer_address");
                                    }
                                    ?>
                                    {!! Form::text('customer_address',$cus_address, array('class' => 'validate[required] form-control', 'id'=>'customer_address','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_country', ' Country:', array('for' => 'customer_country'))!!}
                                </div>
                                <div class="col-md-9">
                                    <?php
                                    if($customer_result->country){
                                        $cus_country = $customer_result->country;
                                    }else{
                                        $cus_country = Input::get("customer_country");
                                    }
                                    ?>
                                    {!! Form::text('customer_country',$cus_country, array('class' => 'validate[required] form-control', 'id'=>'customer_country','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_email', ' Email:', array('for' => 'customer_email'))!!}
                                </div>
                                <div class="col-md-9">

                                    <?php
                                    if($customer_result->email){
                                        $cus_email = $customer_result->email;
                                    }else{
                                        $cus_email = Input::get("customer_email");
                                    }
                                    ?>

                                    {!! Form::text('customer_email',$cus_email, array('class' => 'validate[required] form-control', 'id'=>'customer_email','placeholder'=>''))!!}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_date_of_birth', ' Date of Birth:', array('for' => 'customer_date_of_birth'))!!}
                                </div>
                                <div class="col-md-9">

                                    <?php
                                    if($customer_result->date_of_birth){
                                        $cus_date_of_birth = $customer_result->date_of_birth;
                                    }else{
                                        $cus_date_of_birth = Input::get("customer_date_of_birth");
                                    }
                                    ?>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="i-calendar"></i></span>
                                        {!! Form::text('customer_date_of_birth',$cus_date_of_birth, array('class' => 'datepicker form-control', 'id'=>'customer_date_of_birth','placeholder'=>''))!!}
                                    </div>
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_mobile', ' Telephone -  Mobile:', array('for' => 'customer_mobile'))!!}
                                </div>
                                <div class="col-md-9">

                                    <?php
                                    if($customer_result->phone_mobile){
                                        $cus_phone_mobile = $customer_result->phone_mobile;
                                    }else{
                                        $cus_phone_mobile = Input::get("customer_mobile");
                                    }
                                    ?>

                                    {!! Form::text('customer_mobile',$cus_phone_mobile, array('class' => 'mask_phone validate[required] form-control', 'id'=>'customer_mobile','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>



                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_home', ' Telephone - Home:', array('for' => 'customer_home'))!!}
                                </div>
                                <div class="col-md-9">
                                    <?php
                                    if($customer_result->phone_home){
                                        $cus_phone_home = $customer_result->phone_home;
                                    }else{
                                        $cus_phone_home = Input::get("customer_home");
                                    }
                                    ?>
                                    {!! Form::text('customer_home',$cus_phone_home, array('class' => 'mask_phone form-control', 'id'=>'customer_home','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>




                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_created_by', ' Created By:', array('for' => 'customer_created_by'))!!}
                                </div>
                                <div class="col-md-9">
                                    <?php $created_admin_name =  Auth::user()->name;  ?>
                                    {!! Form::text('customer_created_by',$created_admin_name, array('class' => 'form-control', 'id'=>'customer_created_by', 'disabled'=>'disabled'))!!}
                                </div>
                            </div>




                        </div>

                        <div class="footer">
                            <div class="side fr">
                                {!! Form::hidden('customer_edit', '1') !!}
                                {!! Form::hidden('customer_id', $customer_result->id) !!}
                                {!! Form::hidden('customer_created_admin_id', Auth::user()->id) !!}
                                {!! Form::button('Update',array('type'=>'submit','class'=>'btn btn-primary','value'=>'customer_submit','name'=>'customer_submit')) !!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>

                </div>

            </div>






        </div>

    </div>




@stop