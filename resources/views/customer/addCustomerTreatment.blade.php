@extends('layouts.master')

@section('title', 'Add a customer treatment ')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Customer Treatment   </h1>
                <ul class="breadcrumb">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    @if(session()->get('role')==1)
                        <li><a href="{{ url('customer/customer_treatments') }}">Customer Treatment </a></li>
                    @endif

                    <li class="active">Create Customer Treatment </li>
                </ul>
            </div>


        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="block">
                        <div class="head">
                            <h2>Create a new customer treatment  </h2>
                            <div class="side fr">

                            </div>
                        </div>
                        <div class="content np">

                            {!!Form::open(array('action' => 'CustomerController@createCustomerTreatment','id' => 'validate',  'files'=>true,'class' => 'create_customer_treatment','role'=>'form','onclick'=>'javascript'))!!}

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_treatment_date', ' Treatment Date:', array('for' => 'customer_treatment_date'))!!}
                                </div>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="i-calendar"></i></span>
                                        {!! Form::text('customer_treatment_date',Input::get("customer_treatment_date"), array('class' => 'datepicker validate[required] form-control', 'id'=>'customer_treatment_date','placeholder'=>''))!!}
                                    </div>
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_treatment_for', ' Treatment For:', array('for' => 'customer_treatment_for'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('customer_treatment_for',Input::get("customer_treatment_for"), array('class' => 'validate[required] form-control', 'id'=>'customer_treatment_for','placeholder'=>''))!!}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_treatment_description', ' Treatment Description:', array('for' => 'customer_treatment_description'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('customer_treatment_description',Input::get("customer_treatment_description"), array('class' => 'validate[required] form-control', 'id'=>'customer_treatment_description','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_treatment_before_image', ' Before Treatment:', array('for' => 'customer_treatment_created_by'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::file('customer_treatment_before_image', array('class' => 'uni', 'id'=>'customer_treatment_before_image'))!!}

                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('customer_treatment_after_image', ' After Treatment:', array('for' => 'customer_treatment_created_by'))!!}
                                </div>
                                <div class="col-md-9">

                                    <select name="treatment_id" id="treatment_id" class="validate[required] form-control">
                                        <option value="">Select a treatment</option>
                                        @foreach($treatment_list as $treatment_value)
                                        <option value="{{ $treatment_value->id }}">{{ $treatment_value->treatment_type }}</option>
                                         @endforeach
                                    </select>

                                </div>
                            </div>

                            @if(session()->get('role')==1)
                            <div class="controls-row">

                                <div class="col-md-3">
                                    {!! Form::label('customer_treatment_created_by', ' Created By:', array('for' => 'customer_treatment_created_by'))!!}
                                </div>

                                <div class="col-md-9">
                                    <?php $created_admin_name =  Auth::user()->name;  ?>
                                    {!! Form::text('customer_treatment_created_by',$created_admin_name, array('class' => 'form-control', 'id'=>'customer_treatment_created_by', 'disabled'=>'disabled'))!!}
                                </div>

                            </div>
                            @endif




                        </div>

                        <div class="footer">
                            <div class="side fr">
                                {!! Form::hidden('customer_treatment_create', '1') !!}
                                {!! Form::hidden('customer_id', $customer_id) !!}
                                @if(session()->get('role')==1)
                                {!! Form::hidden('customer_treatment_created_admin_id', Auth::user()->id) !!}
                                @endif
                                {!! Form::button('Create a customer treatment',array('type'=>'submit','class'=>'btn btn-primary','value'=>'customer_treatment_submit','name'=>'customer_treatment_submit')) !!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>

                </div>

            </div>






        </div>

    </div>




@stop