 <?php
 //var_dump($customer_treatments_details[0]);
       // exit;
 ?>
@extends('layouts.master')

@section('title', 'View Customer Treatment Details')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Customer  </h1>
                <ul class="breadcrumb">

                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    @if(session()->get('role')==1)
                        <li><a href="{{ url('customer') }}">  Customer Treatment</a></li>
                    @endif

                    <li class="active">View Customer Treatment Details</li>
                </ul>
            </div>

        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong>Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif



                    <div class="row">

                        <div class="col-md-12">
                            <div class="block">
                                <div class="head">
                                    <h2>Customer Treatment Details</h2>
                                </div>
                                <div class="content np">

                                    <div class="controls-row">
                                        <div class="col-md-4">Full Name:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">{{ $customer_treatments_details[0]->title ." : ".$customer_treatments_details[0]->first_name ." ".$customer_treatments_details[0]->last_name  }}</p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Treatment :</div>
                                        <div class="col-md-8">
                                            <p class=""> {{ $customer_treatments_details[0]->treatment_type  }}</p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Treatment Date:</div>
                                        <div class="col-md-8">
                                            <?php
                                            $created_date = explode(" ",$customer_treatments_details[0]->treatment_date);
                                            ?>
                                            <p class="doctor_details_p">{{ $created_date[0]  }}</p>
                                        </div>
                                    </div>



                                    <div class="controls-row">
                                        <div class="col-md-4">Treatment For:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">  {{ $customer_treatments_details[0]->treatment_for  }} </p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Treatment Description:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p"> {{ $customer_treatments_details[0]->treatment_description  }} </p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Before Treatment</div>
                                        <div class="col-md-2">

                                          <?php //$path = storage_path('app/uploads/before_treatment/'.$customer_treatments_details[0]->before_treatment_image); ?>

                                          <?php
                                           //$web_root = "http://".$_SERVER['HTTP_HOST']."/verdict/treatment/storage/app/uploads/before_treatment/".$customer_treatments_details[0]->before_treatment_image;
                                          $web_root = "http://".$_SERVER['HTTP_HOST'].env('BEFORE_TREATMENT_FILE_DIR_PATH').$customer_treatments_details[0]->before_treatment_image;
                                          ?>
                                                <a class="fancybox" rel="group" href="{{ $web_root   }}"><img src="{{ $web_root  }}" class="img-thumbnail"/></a>

                                            </div>
                                    </div>




                                </div>
                            </div>
                        </div>



                    </div>






                </div>






            </div>

        </div>



@stop
        