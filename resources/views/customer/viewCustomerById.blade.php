@extends('layouts.master')

@section('title', 'View Customer Details')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Customer  </h1>
                <ul class="breadcrumb">

                    @if(session()->get('role')==1)
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    @endif

                    @if(session()->get('role')==1)
                        <li><a href="{{ url('customer') }}">Customer</a></li>
                    @endif

                    <li class="active">View Customer</li>
                </ul>
            </div>

        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong>Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif

                        <div class="success_msg_append" style="display: none"> </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="block">
                                <div class="head">
                                    <h2>Customer Personal details</h2>
                                    <ul class="buttons">
                                        <li><a href="#" class="block_loading"><span class="i-cycle"></span></a></li>
                                        <li><a href="#" class="block_toggle"><span class="i-arrow-down-3"></span></a></li>
                                        <li><a href="#" class="block_remove"><span class="i-cancel-2"></span></a></li>
                                    </ul>
                                </div>
                                <div class="content np">
                                    {{--<div class="controls-row">--}}
                                        {{--<div class="col-md-1">--}}
                                            {{--<img src="{{url('assets/img/cus_icon.png')}}" class="img-thumbnail" style="margin-bottom: 5px;">--}}
                                        {{--</div>--}}

                                    {{--</div>--}}
                                    <div class="controls-row">
                                        <div class="col-md-4">Full Name:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">{{ $customer_details->title ." : ".$customer_details->first_name ." ".$customer_details->last_name  }}</p>
                                        </div>
                                    </div>



                                    <div class="controls-row">
                                        <div class="col-md-4">Address:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">{{ $customer_details->address  }}</p>
                                        </div>
                                    </div>



                                    <div class="controls-row">
                                        <div class="col-md-4">Country:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">  {{ $customer_details->country  }} </p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Date of birth:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p"> {{ $customer_details->date_of_birth  }} </p>
                                        </div>
                                    </div>




                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="block">
                                <div class="head">
                                    <h2>Customer Contact details</h2>
                                    <ul class="buttons">
                                        <li><a href="#" class="block_loading"><span class="i-cycle"></span></a></li>
                                        <li><a href="#" class="block_toggle"><span class="i-arrow-down-3"></span></a></li>
                                        <li><a href="#" class="block_remove"><span class="i-cancel-2"></span></a></li>
                                    </ul>
                                </div>
                                <div class="content np">


                                    <div class="controls-row">
                                        <div class="col-md-4">E-Mail:</div>
                                        <div class="col-md-8">
                                            <p class=""> {{ $customer_details->email  }}</p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Phone - Mobile:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">  {{ $customer_details->phone_mobile  }} </p>
                                        </div>
                                    </div>


                                    <div class="controls-row">
                                        <div class="col-md-4">Phone - Home:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">{{ $customer_details->phone_home }} </p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Created at :</div>
                                        <div class="col-md-8">
                                            <?php

                                            $created_date = explode(" ",$customer_details->created_at);
                                            ?>
                                            <p class="doctor_details_p">{{ $created_date[0] }} </p>
                                        </div>
                                    </div>


                                </div>

                            </div>


                        </div>


                    </div>





                    <div class="row">



                        <div class="col-md-12">
                            <div class="block">
                                <div class="head">
                                    <h2>Treatments</h2>
                                    <ul class="buttons">
                                        <li><a href="#" class="block_loading"><span class="i-cycle"></span></a></li>
                                        <li><a href="#" class="block_toggle"><span class="i-arrow-down-3"></span></a></li>
                                        <li><a href="#" class="block_remove"><span class="i-cancel-2"></span></a></li>
                                    </ul>
                                    <div class="side fr">
                                        <span class="label label-danger">Total : {{ count($customer_treatments_details) }}</span>
                                    </div>
                                </div>

                                <div class="content np table-sorting">

                                    <table cellpadding="0" cellspacing="0" width="100%" class="simple_sort">
                                        <thead>
                                        <tr>
                                            <th width="10%">Date</th>
                                            <th width="10%">Treatment</th>
                                            <th width="40%">For</th>
                                            <th width="2%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($customer_treatments_details as $customer_treatments_value)
                                            <tr id="row_subtuid_{{$customer_treatments_value->id}}">
                                                <td>
                                                    <?php $customer_treatments_date = explode(" ",$customer_treatments_value->treatment_date); ?>
                                                    <span class="date">{{ $customer_treatments_date[0]   }}</span>
                                                </td>
                                                <td>
                                                    <a href="{{url('customer/customer_treatments_details/'.$customer_treatments_value->id)}}" style="text-transform: capitalize;">
                                                        {{ $customer_treatments_value->treatment_type }}
                                                    </a>
                                                </td>
                                                <td>{{ $customer_treatments_value->treatment_for }}</td>
                                                <td>
                                                    <a href="{{url('customer/customer_treatments_details/'.$customer_treatments_value->id)}}" style="text-transform: capitalize;">
                                                        <span class=" i-file text-success"></span>
                                                    </a>
                                                    <a href="#bModal" data-toggle="modal" id="{{$customer_treatments_value->id}}" onclick="return  setTreatmentById('{{csrf_token()}}','{{$customer_treatments_value->id}}','{{$customer_treatments_value->before_treatment_image}}');" class="delete_customer_model">
                                                        <span class="i-trashcan text-danger"></span>
                                                    </a>
                                                </td>

                                            </tr>



                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="4" align="right">
                                                {{--<button class="btn btn-sm btn-primary">More...</button>--}}
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>



                </div>






            </div>

        </div>


        <!-- Bootrstrap modal -->
        <div class="modal fade" id="bModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel2">Delete Customer Treatment</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure want to Delete this Customer Treatment ?</p>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" class="treatment_id" id="set_treatment_id"/>
                        <input type="hidden" class="set_before_treatment_image" id="set_before_treatment_image"/>
                        <a href="#" class="btn btn-danger" id="delete_cus" data-dismiss="modal" aria-hidden="true" onclick="return  deleteCustomerTreatmentById('{{csrf_token()}}');" >Delete</a>
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">


            function setTreatmentById(token,treatment_id,before_image) {
                if(treatment_id){
                    $('#set_treatment_id').val(treatment_id);
                    $('#set_before_treatment_image').val(before_image);
                }else{
                    return false;
                }
            }

            function deleteCustomerTreatmentById(token) {

                var treatment_id = $('#set_treatment_id').attr('value');
                var set_before_treatment_image = $('#set_before_treatment_image').attr('value');

                $.post('{{url('customer/delete_customer_treatment')}}', {treatment_id: treatment_id, 'set_before_treatment_image':set_before_treatment_image,'_token': token}, function (data) {

                    if(data){

                        $('#row_subtuid_'+data).fadeOut(1000);

                        $('.success_msg_append').html(" <div class='alert alert-success'> <strong class='success_msg'> Delete has been success </strong> <button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
                        $('.success_msg_append').css('display','block');
                        $('.success_msg_append').fadeOut(6000);

                        location.reload();

                    }else{
                        return false;
                    }

                });
            }

    </script>

@stop