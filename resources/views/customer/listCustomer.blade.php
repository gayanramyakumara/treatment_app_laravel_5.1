@extends('layouts.master')

@section('title', 'List Customers')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Customers  </h1>
                <ul class="breadcrumb">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    <li><a href="{{ url('customer') }}">Customers</a></li>
                    <li class="active">Show Customers</li>
                </ul>
            </div>

            <div class="search">
                {!!Form::open(array('action' => 'CustomerController@findCustomerByName','id' => 'find_customer_by_name', 'class' => 'form','role'=>'form'))!!}

                {!! Form::text('customer_name',Input::get("customer_name"), array('class' => 'form-control', 'placeholder'=>'Customer Name...','style'=>'color: #67667B;font-size: 14px;'))!!}

                <button type="submit"><span class="i-magnifier"></span></button>

                {!!Form::close()!!}
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong class="success_msg">Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif

                        <div class="success_msg_append" style="display: none"> </div>

                    <div class="block">



                        <div class="head">
                            <h2>Show All Customers  </h2>
                            <div class="side fl">
                                <a href="{{ url('customer/create') }}" class="btn btn-success "><span class=" i-user"></span></a>

                            </div>
                        </div>

                        <div class="content np">

                            <div class="">

                                <table width="100%" >
                                {{--<table cellpadding="0" cellspacing="0" width="100%" class="simple_sort" >--}}
                                    <thead>
                                    <tr>

                                        <th width="5%">ID</th>
                                        <th width="15%">Name</th>
                                        <th width="15%">address</th>
                                        <th width="15%">Phone</th>
                                        <th width="15%">email</th>
                                        <th width="10%"> </th>
                                        <th width="5%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($customers as $customers_value)

                                        <tr id="row_subtuid_{{$customers_value->id}}">
                                            <td><input type="checkbox" name="checkbox"/></td>
                                            <td>
                                                <a href="{{url('customer/view/'.$customers_value->id)}}" style="text-transform: capitalize;">
                                                {{$customers_value->title." ".$customers_value->first_name." ".$customers_value->last_name}}
                                                </a>
                                            </td>

                                            <td>{{$customers_value->address. " ".$customers_value->country}}</td>
                                            <td>  {{$customers_value->phone_mobile ." / ".$customers_value->phone_home}}</td>

                                            <td>{{$customers_value->email}}</td>
                                            <td>

                                                <a href="{{ url('customer/create_customer_treatment/'.$customers_value->id) }}" class="btn btn-sm btn-info">Add A Treatment</a>
                                            </td>
                                            <td>

                                                <a href="{{url('customer/edit/'.$customers_value->id)}}">
                                                    <span class="i-pencil text-danger"></span>
                                                </a>

                                                <a href="{{url('customer/view/'.$customers_value->id)}}">
                                                    <span class=" i-file text-success"></span>
                                                </a>

                                                <a href="#bModal" data-toggle="modal" id="{{$customers_value->id}}" onclick="return  deleteCustomerById('{{csrf_token()}}','{{$customers_value->id}}')" class="delete_customer_model">
                                                    <span class="i-trashcan text-danger"></span>
                                                </a>

                                            </td>
                                        </tr>

                                    @endforeach






                                    </tbody>
                                </table>


                            </div>



                        </div>

                        <div>

                        </div>



                    </div>

                </div>


                <div class="col-md-4"></div>

                <div class="col-md-4">
                    {!! $customers->render() !!}
                </div>


                <div class="col-md-4"></div>


            </div>

        </div>



        <!-- EOF Bootrstrap default dialog -->
        <!-- Bootrstrap modal -->
        <div class="modal fade" id="bModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel2">Delete Customer</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure want to Delete this customer ?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-danger" id="delete_cus" data-dismiss="modal" aria-hidden="true">Delete</a>
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>




        <script type="text/javascript">

            function deleteCustomerById(token,cus_id) {



                //  var customer_id = $(this).attr('id');
                //  alert(cus_id);

                $('#delete_cus').click(function () {


                    $.post('{{url('customer/delete')}}', {customer_id: cus_id, '_token': token}, function (data) {


                     if(data){

                         $('#row_subtuid_'+data).fadeOut(1000);



                         $('.success_msg_append').html(" <div class='alert alert-success'> <strong class='success_msg'> Delete has been success </strong> <button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
                         $('.success_msg_append').css('display','block');
                         $('.success_msg_append').fadeOut(6000);
                         location.reload();

                      }else{
                         return false;
                       }


                    });


                });
            }

        </script>

@stop