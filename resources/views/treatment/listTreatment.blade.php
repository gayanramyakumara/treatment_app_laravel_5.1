@extends('layouts.master')

@section('title', 'List Treatments')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Treatments  </h1>
                <ul class="breadcrumb">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    <li><a href="{{ url('customer') }}">Treatments</a></li>
                    <li class="active">Show Treatment</li>
                </ul>
            </div>

            <div class="search">
                {!!Form::open(array('action' => 'TreatmentController@findTreatmentByName','id' => 'find_customer_by_name', 'class' => 'form','role'=>'form'))!!}

                {!! Form::text('treatment_name',Input::get("treatment_name"), array('class' => 'form-control', 'placeholder'=>'Treatment Name...','style'=>'color: #67667B;font-size: 14px;'))!!}

                <button type="submit"><span class="i-magnifier"></span></button>

                {!!Form::close()!!}

            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-6">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong class="success_msg">Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif

                    <div class="success_msg_append" style="display: none"> </div>

                    <div class="block">



                        <div class="head">
                            <h2>Show All Treatments  </h2>
                            <div class="side fl">
                                <a href="{{ url('treatment/create') }}" class="btn btn-success "><span class=" i-user"></span></a>

                            </div>
                        </div>

                        <div class="content np">

                            <div class="content np table-sorting">

                                <table cellpadding="0" cellspacing="0" width="100%" class="simple_sort">
                                    <thead>
                                    <tr>

                                        <th width="5%">ID</th>
                                        <th width="25%">Treatment Name</th> 
                                        <th width="5%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($treatment as $treatment_value)

                                        <tr id="row_subtuid_{{$treatment_value->id}}">

                                            <td><input type="checkbox" name="checkbox"/></td>

                                            <td>{{$treatment_value->treatment_type}}</td>
                                            <td>

                                                <a href="#fModal" role="button" class="btn " id="{{$treatment_value->id}}" data-toggle="modal" onclick="return  findTreatmentById('{{csrf_token()}}','{{$treatment_value->id}}')">
                                                    <span class="i-pencil text-danger"></span>
                                                </a>



                                                <a href="#bModal" data-toggle="modal" id="{{$treatment_value->id}}" onclick="return  deleteTreatmentById('{{csrf_token()}}','{{$treatment_value->id}}')" class="delete_customer_model">
                                                    <span class="i-trashcan text-danger"></span>
                                                </a>

                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>
                                </table>

                            </div>

                        </div>

                    </div>

                </div>






            </div>

        </div>



        <!--  Update Treatment Bootrstrap modal form -->

        <div class="modal fade" id="fModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel3">Update Treatment</h3>
                    </div>

                    {!!Form::open(array('action' => 'TreatmentController@update','id' => 'validate', 'class' => 'create_new_treatment','role'=>'form','onclick'=>'javascript'))!!}


                    <div class="modal-body">
                        <div class="row">


                                <div class="controls-row">
                                    <div class="col-md-3">
                                        {!! Form::label('treatment_name', ' Treatment Name:', array('for' => 'treatment_name'))!!}
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::text('treatment_name',Input::get("treatment_name"), array('class' => 'validate[required] form-control', 'id'=>'form_treatment_name','placeholder'=>''))!!}
                                    </div>
                                </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--'onclick'=>'return  findTreatmentById("{{csrf_token()}}")'--}}
                        <?php $token = csrf_token(); ?>
                        {!! Form::button('Update treatment',array('type'=>'submit','class'=>'btn btn-info','aria-hidden'=>'true','data-dismiss'=>'modal','value'=>'treatment_submit','id'=>'treatment_submit','name'=>'treatment_submit','onclick'=>"return updateTreatmentById('$token')")) !!}
                        {!! Form::hidden('form_treatment_id', '',array('id'=>'form_treatment_id')) !!}
                        {!! Form::hidden('treatment_edit', '1',array('id'=>'treatment_edit')) !!}
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>

                    {!!Form::close()!!}

                </div>
            </div>
        </div>
        <!-- End Update Treatment Bootrstrap modal form -->


        <!-- Delete Bootrstrap modal -->
        <div class="modal fade" id="bModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel2">Delete Customer</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure want to Delete this treatment ?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-danger" id="delete_cus" data-dismiss="modal" aria-hidden="true">Delete</a>
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Delete Bootrstrap modal -->


        <script type="text/javascript">

            function deleteTreatmentById(token,treat_id) {


                $('#delete_cus').click(function () {


                    $.post('{{url('treatment/delete')}}', {treat_id: treat_id, '_token': token}, function (data) {

                        if(data){

                            $('#row_subtuid_'+data).fadeOut(1000);



                            $('.success_msg_append').html(" <div class='alert alert-success'> <strong class='success_msg'> Delete has been success </strong> <button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
                            $('.success_msg_append').css('display','block');
                            $('.success_msg_append').fadeOut(6000);
                            location.reload();

                        }else{
                            return false;
                        }


                    });


                });
            }


            function findTreatmentById(token,treat_id) {

                $.post('{{url('treatment/details')}}',{treat_id: treat_id, '_token': token}, function (data) {

                    if(data['id']){

                        $('#form_treatment_name').val(data['treatment_type']);
                        $('#form_treatment_id').val(data['id']);

                    }else{
                        return false;
                    }
                    return false;

                });
            }

            function updateTreatmentById(token){

                    var treatment_name  =  $('#form_treatment_name').val();
                    var treat_id    =  $('#form_treatment_id').val();
                    var treatment_edit  =  $('#treatment_edit').val();

                    $.post('{{url('treatment/update')}}',{treat_id: treat_id,treatment_edit:treatment_edit,treatment_name:treatment_name, '_token': token}, function (data) {

                        $('.success_msg_append').html(" <div class='alert alert-success'> <strong class='success_msg'> Update has been success </strong> <button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
                        $('.success_msg_append').css('display','block');
                        $('.success_msg_append').fadeOut(6000);

                        location.reload();

                        return false;

                    });

            }



        </script>

@stop