@extends('layouts.master')

@section('title', 'Add treatment')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Treatment  </h1>
                <ul class="breadcrumb">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    <li><a href="{{ url('treatment') }}">Treatment</a></li>
                    <li class="active">Create Treatment</li>
                </ul>
            </div>


        </div>

        <div class="container">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>

                            <li>{{ $errors }}</li>

                    </ul>
                </div>
            @endif



            <div class="row">

                <div class="col-md-6">

                    <div class="block">
                        <div class="head">
                            <h2>Create a new treatment  </h2>
                            <div class="side fr">

                            </div>
                        </div>
                        <div class="content np">

                            {!!Form::open(array('action' => 'TreatmentController@create','id' => 'validate', 'class' => 'create_new_treatment','role'=>'form','onclick'=>'javascript'))!!}

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('treatment_name', ' Treatment Name:', array('for' => 'treatment_name'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('treatment_name',Input::get("treatment_name"), array('class' => 'validate[required] form-control', 'id'=>'treatment_name','placeholder'=>''))!!}
                                </div>
                            </div>

                        </div>

                        <div class="footer">
                            <div class="side fr">
                                {!! Form::hidden('treatment_create', '1') !!}
                                {!! Form::button('Create a treatment',array('type'=>'submit','class'=>'btn btn-primary','value'=>'treatment_submit','name'=>'treatment_submit')) !!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>

                </div>

            </div>






        </div>

    </div>




@stop