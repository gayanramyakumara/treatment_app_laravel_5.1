<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTreatmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('customer_treatment', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('treatment_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->dateTime('treatment_date');
            $table->text('treatment_for');
            $table->text('treatment_description');
            $table->text('before_treatment_image');
            $table->text('after_treatment_image');

            $table->foreign('treatment_id')->references('id')->on('treatment');
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_treatment');
    }
}
