-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2015 at 05:06 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `treatment_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_mobile` int(11) NOT NULL,
  `phone_home` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `admin_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `title`, `first_name`, `last_name`, `address`, `country`, `date_of_birth`, `phone_mobile`, `phone_home`, `email`, `password`, `is_active`, `admin_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(10, 'mrs', 'Gayan', 'Ramya Kumara', 'Galle Rd, Dehiwala', 'Sri Lanka', '12/29/2015', 1242346453, 2147483647, 'abc@test.lk', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'mr', 'Teren', 'Aluthwala', 'Kadawatha Road,Ragama', 'Sri Lanka', '12/21/2015', 1242346453, 2147483647, 'info@teran.lk', '', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'miss', 'Thilanka', 'Krishanthi', 'Matugama Road,Kalutara', 'Sri Lanka', '12/30/2015', 771237627, 384567777, 'hr@thilanka.lk', 'bfbce6e753c0ae8daff71ed896290ce5', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'mr', 'Kalum', 'Sudarshana', 'Galle Rd, Rathmalana', 'Sri Lanka', '16/29/2015', 1242346453, 2147483647, 'info@sudarshana.lk', '2fdcb05c1aa0b960ad91b3f2d9f6e718', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'mr', 'Jayan', 'Siriwardhana', 'Dalugama Road,Kalaniya', 'Sri Lanka', '11/11/2015', 1242346453, 2147483647, 'info@siriwardhana.lk', '', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'miss', 'Sulochana', 'Rathnayeka', 'Matugama Road,Agalawatta', 'Sri Lanka', '12/30/2015', 771237627, 384567777, 'hr@rathnayeka.lk', 'bfbce6e753c0ae8daff71ed896290ce5', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'miss', 'test', 'sjdh', 'Kadawatha Road,Ragama', 'Sri Lanka', '12/28/2015', 2147483647, 1234567890, 'gayan@86.lk', '526c71b4dd58e562b6bcc82bf78e9c08', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'mr', 'gayan Kalum', 'Suranga', 'Kadawatha Road,Ragama', 'Sri Lanka', '12/30/2015', 2147483647, 2147483647, 'gayan@222223.lk', '625024faca2c66ae306b238a578bcd79', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_treatment`
--

CREATE TABLE IF NOT EXISTS `customer_treatment` (
`id` int(10) unsigned NOT NULL,
  `treatment_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `treatment_date` datetime NOT NULL,
  `treatment_for` text COLLATE utf8_unicode_ci NOT NULL,
  `treatment_description` text COLLATE utf8_unicode_ci NOT NULL,
  `before_treatment_image` text COLLATE utf8_unicode_ci NOT NULL,
  `after_treatment_image` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_treatment`
--

INSERT INTO `customer_treatment` (`id`, `treatment_id`, `customer_id`, `treatment_date`, `treatment_for`, `treatment_description`, `before_treatment_image`, `after_treatment_image`, `created_at`, `updated_at`) VALUES
(6, 5, 15, '2015-12-15 00:00:00', 'Dapibus Ac Facilisis In, Egestas Eget Quam', ' Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli. Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli. Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli', 'img_before_date35302.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 5, 15, '2015-12-22 00:00:00', 'Dapibus Ac Facilisis In, Egestas Eget Quam', ' Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli. Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli. Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli', 'img_before_date50930.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 3, 10, '2015-12-07 00:00:00', 'Dapibus Ac Facilisis In, Egestas Eget Quam', ' Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli. Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli. Cras Justo Odio, Dapibus Ac Facilisis In, Egestas Eget Quam. Donec Id Elit Non Mi Porta Gravida At Eget Metus. Nullam Id Dolor Id Nibh Ultricies Vehicula Ut Id Eli', 'img_before_date77031.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 3, 10, '2015-12-01 00:00:00', 'test avc', 'test test etstssasd afew', 'img_before_date97664.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 4, 10, '2015-12-15 00:00:00', 'bbbb', 'asas', 'img_before_date96609.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 5, 10, '2015-12-22 00:00:00', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. ', 'img_before_date99899.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 5, 10, '2015-12-28 00:00:00', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.', 'img_before_date73936.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 6, 10, '2015-12-29 00:00:00', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.', 'img_before_date59247.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_11_165416_create_customer_table', 1),
('2015_12_14_101451_create_treatment_table', 2),
('2015_12_15_072835_create_customer_treatment_table', 3),
('2015_12_22_062435_create_sessions_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `treatment`
--

CREATE TABLE IF NOT EXISTS `treatment` (
`id` int(10) unsigned NOT NULL,
  `treatment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `treatment`
--

INSERT INTO `treatment` (`id`, `treatment_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Treatment 1', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Treatment 2', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Treatment 3', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Treatment 4', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Treatment 5', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Treatment 6', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Gayan Ramya Kumara', 'gayanramyakumara@gmail.com', '$2y$10$g1eKeWrA/M6EbvTPpfBvH.dqvD41UYShXCjw62YDVg8foabAsVOwK', 'ds4xi113aMV5srYywUBhnSxTAvYRE6tWuljqKrdvEDJWfX4mEi55AIXp9B2x', '2015-12-11 11:51:20', '2015-12-22 01:57:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `customer_email_unique` (`email`), ADD KEY `customer_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `customer_treatment`
--
ALTER TABLE `customer_treatment`
 ADD PRIMARY KEY (`id`), ADD KEY `customer_treatment_treatment_id_foreign` (`treatment_id`), ADD KEY `customer_treatment_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
 ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `treatment`
--
ALTER TABLE `treatment`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `treatment_type` (`treatment_type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `customer_treatment`
--
ALTER TABLE `customer_treatment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `treatment`
--
ALTER TABLE `treatment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
ADD CONSTRAINT `customer_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `customer_treatment`
--
ALTER TABLE `customer_treatment`
ADD CONSTRAINT `customer_treatment_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
ADD CONSTRAINT `customer_treatment_treatment_id_foreign` FOREIGN KEY (`treatment_id`) REFERENCES `treatment` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
